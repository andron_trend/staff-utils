<?php
/*
* Сервис удаляет записи истрории изменений персональных данных из коллекции STAFF_HISTORY
* по дефолту старше 60 суток ( $days = intval(60) ) либо числу суток указанных в первым
* аргументом в коммандной строке
*/
include 'Config.php';
error_reporting(E_ERROR); // только фатальные 

$manager = new MongoDB\Driver\Manager('mongodb://'. Config::MON_USER . ':' . Config::MON_PWD . '@' . Config::MON_HOST);
$bulk = new MongoDB\Driver\BulkWrite(['ordered' => true]);
$bulk2 = new MongoDB\Driver\BulkWrite(['ordered' => true]);
$wc = new MongoDB\Driver\WriteConcern(MongoDB\Driver\WriteConcern::MAJORITY, 1000);

if (isset($argv[1])){
    if($argv[1]){
        $days = intval($argv[1]);
    } else {
        $days = intval(60);
    }
}

$interval = 'P'.intval($days).'D';
$now = new DateTime();
$now = $now->sub(new DateInterval($interval));
$st = strtotime($now->format('Y-m-d'));
$cond = dechex($st) . "0000000000000000" ;
echo "Clear history at " . $cond . "\n";

$bulk->delete(['_id' => ['$lt' => new MongoDB\BSON\ObjectId("$cond")]]);
$result = $manager->executeBulkWrite('personal.staff_history', $bulk, $wc);
echo "Cleared history " . $result->getDeletedCount() . " documents \n";

$expired_time = new MongoDB\BSON\UTCDateTime($now);
$bulk2->delete(['expired_time' => ['$lt' => $expired_time]]);
$result = $manager->executeBulkWrite('personal.auth', $bulk2, $wc);
echo "Cleared " . $result->getDeletedCount() . " tokens \n";
