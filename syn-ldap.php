<?php
include 'Config.php';
    error_reporting(E_ERROR); // только фатальные 

$now = new DateTime();
$manager = new MongoDB\Driver\Manager('mongodb://'. Config::MON_USER . ':' . Config::MON_PWD . '@' . Config::MON_HOST);
$bulk = new MongoDB\Driver\BulkWrite(['ordered' => true]);
$bulk_h = new MongoDB\Driver\BulkWrite(['ordered' => true]);
$wc = new MongoDB\Driver\WriteConcern(MongoDB\Driver\WriteConcern::MAJORITY, 1000);
$wc_h = new MongoDB\Driver\WriteConcern(MongoDB\Driver\WriteConcern::MAJORITY, 1000);

$mydb = new PDO('mysql:host='. Config::MY_HOST, Config::MY_USER, Config::MY_PWD);
$mydb->exec("SET NAMES 'UTF8'");
$mysql = "UPDATE staff SET ad_acc = :ACC WHERE _id = :ID";
$my = $mydb->prepare($mysql);

$ldap = ldap_connect(Config::AD_HOST,Config::AD_PORT) or die("Cant connect to LDAP Server");

echo $now->format('Y-m-d e H:i:s') . "\n";
echo "STAFF sync data to AD \n";
echo "----------------------------------------\n";


$i = 0;
define('U_ACCOUNT_DISABLE', 1 << 1);   // 0010
ldap_set_option($ldap, LDAP_OPT_PROTOCOL_VERSION, 3);
ldap_set_option($ldap, LDAP_OPT_REFERRALS, 0);
$justthese = array("dn", "sAMAccountName", "employeeNumber", 'userAccountControl');

$bind = ldap_bind($ldap,Config::AD_USER, Config::AD_PWD) or die("Cant bind to LDAP Server");
$changes = false;

foreach(range('a','z') as $j){
    $out = ldap_search($ldap,Config::AD_BASE,"(&(objectClass=user)(objectCategory=Person)(employeeNumber=*)(cn={$j}*))",$justthese,0);
    if ($out){
        $dn = ldap_get_entries($ldap, $out);
        foreach( $dn as $row) {
            
            if ( !($row['useraccountcontrol'][0] & U_ACCOUNT_DISABLE)) { 
                //echo $row['employeenumber'][0]; 
                $id = intval($row['employeenumber'][0]);
                // echo $id . "\n";
                $query = new MongoDB\Driver\Query(["_id" =>  $id]);
                $cursor = $manager->executeQuery('personal.staff', $query)->toArray();
                    if ( count($cursor) != 0){
                        if (isset($cursor[0]->ad_acc)) {
                            $dst = $cursor[0]->ad_acc;
                            $dst_name = $cursor[0]->name_short;
                        } else  {
                            $dst = null;
                        }
                        $src = $row['samaccountname'][0];
                        if ($dst != $src) {
                            // echo "AD:" . $src . ", DOC:" . $dst . "\n";
                            $bulk->update(["_id" => $id], array('$set' => ['ad_acc' => $src]));
                            $bulk_h->insert(array('pn'=> $id, 'name_short'=> $dst_name, 'changed'=>'ad_acc', 'oldvalue' => $dst, 'newvalue' => $src));
                             
                            $my->bindValue(':ID', $id, PDO::PARAM_INT);
                            $my->bindValue(':ACC', mb_convert_encoding($src,'UTF-8','Windows-1251'), PDO::PARAM_STR);
                            if(!$my->execute()){
                               echo "Error update PN=" . $id ."\n";
                            } else { 
                               $i = $i + $my->rowCount();
                            }
                            $changes = true;

                            // $dn = ldap_get_entries($ldap, $ad);
                            $entry["departmentNumber"][] = $cursor[0]->dep_no;
                            $entry["division"][] = $cursor[0]->div_name;
                            $entry["info"][] = $cursor[0]->div_no;
                            $entry["employeeType"][] = $cursor[0]->cat_name;
                            ldap_mod_replace($ldap, $dn[0]['dn'], $entry);
                            unset($entry);
                        }
                    }
            }
        }	
    }
}
if ($changes) {
    $result = $manager->executeBulkWrite('personal.staff', $bulk, $wc);
    $history = $manager->executeBulkWrite('personal.staff_history', $bulk_h, $wc_h);
    echo "  Attr Updated: " . $result->getModifiedCount() . "\n";
    echo "  Record Updated: " . $i . "\n\n\n";
}
