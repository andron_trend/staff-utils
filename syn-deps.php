<?php
    include 'Config.php';
    error_reporting(E_WARNING); // только фатальные 

    // timestamp операции
    $now = new DateTime();
    $now_bs = new MongoDB\BSON\UTCDateTime($now);
    $now_db = $now->format('Ymd');

    // начинать с корня
    $master = 1;
    $i = 0;
    $j = 0;

    $manager = new MongoDB\Driver\Manager('mongodb://'. Config::MON_USER . ':' . Config::MON_PWD . '@' . Config::MON_HOST);
	$bulk = new MongoDB\Driver\BulkWrite(['ordered' => true]);
    $writeConcern = new MongoDB\Driver\WriteConcern(MongoDB\Driver\WriteConcern::MAJORITY, 1000);

    $mydb = new PDO('mysql:host='. Config::MY_HOST, Config::MY_USER, Config::MY_PWD);
    $mydb->exec("SET NAMES 'UTF8'");
    $mysql = "INSERT INTO departments (id, `name`, short_name, parent, ancestors, isdiv, isvalid)
                     VALUES (:ID, :NAME, :SHORT, :PARENT, :ANCESTORS, :ISDIV, :ISVALID) 
                     ON DUPLICATE KEY UPDATE `name` = :NAME, short_name = :SHORT , isdiv = :ISDIV";
    $my = $mydb->prepare($mysql);

    // дата последней синхронизации
    $query = new MongoDB\Driver\Query(["_id" =>  intval(1)]);
    $cursor = $manager->executeQuery('personal.departments', $query);
    foreach ($cursor as $doc){
        $last_check = $doc->checkin->toDateTime()->format('Ymd');
    }
    if (isset($argv[1])){
        if($argv[1] == 'full'){
            // Полное обновление, просматриваем в Oracle все записи старше 1900-01-01
            $last_check = '19000101';
        }
    }
    echo $now->format('Y-m-d e H:i:s') . "\n";
    echo "DEPARTMENT data sync, changed from: " . $last_check . "\n";
    echo "---------------------------------------------------\n";

    $conn = oci_connect(Config::ORA_USER,Config::ORA_PWD,Config::ORA_TNS,'CL8MSWIN1251');
	if (!$conn) {
		$e = oci_error();
        trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
    }
	// Предварительно готовим массив с ID структурных подразделений
    $divsql = "SELECT distinct e.lv as LV FROM EXT_PERSONAL_DELO_P e order by LV";
	$div =  oci_parse($conn, $divsql);
	oci_execute($div);
	oci_fetch_all($div, $res);
	$divlist = $res['LV'];
	
    $sql = "SELECT  level as o_level
                    , a.post_no as dep_no
                    , a.master_post_no as master_div
                    , a.name
                    , a.short_name
                    , a.flag_ceh
                    , SYS_CONNECT_BY_PATH(a.post_no, '/') as path 
                    , CONNECT_BY_ROOT a.post_no as manager 
             FROM EXT_POST_STRUCTURE a 
             WHERE (a.start_date >= TO_DATE(:stdate, 'YYYYMMDD')) AND ((a.end_date is null) OR (a.end_date > sysdate))
             START WITH a.master_post_no = :master
             CONNECT BY PRIOR a.post_no = a.master_post_no";
    $src = oci_parse($conn, $sql);
    oci_bind_by_name($src, ":master", $master);
    oci_bind_by_name($src, ":stdate", $last_check);
    oci_execute($src);
    
    while (($row = oci_fetch_array($src, OCI_ASSOC)) != false) {
        
        $id = intval($row['DEP_NO']);
        $ancestors = array_map('intval', explode('/', '1'.$row['PATH']));
        $master = intval($row['MASTER_DIV']);
        
        if(in_array($id, $divlist)){
            $isdiv = boolval(true);
        } else {
            $isdiv = boolval(false);
        }
        
        // $isdiv = boolval($row['FLAG_CEH']);

            //write to mysql
            $my->bindValue(':ID', $id, PDO::PARAM_INT);
            $my->bindValue(':NAME', mb_convert_encoding($row['NAME'],'UTF-8','Windows-1251'), PDO::PARAM_STR);
            $my->bindValue(':SHORT', mb_convert_encoding($row['SHORT_NAME'],'UTF-8','Windows-1251'), PDO::PARAM_STR);
            $my->bindValue(':PARENT', $master, PDO::PARAM_INT);
            $my->bindValue(':ANCESTORS',  json_encode($ancestors), PDO::PARAM_STR);
            $my->bindValue(':ISDIV', $isdiv, PDO::PARAM_INT);
            $my->bindValue(':ISVALID', 1, PDO::PARAM_INT);
            if(!$my->execute()){
                echo "Error update " . $id ."\n";
                print_r($mydb->errorInfo());
            } else { 
                $i = $i + $my->rowCount(); 
               
            }
                    
        
        $query = new MongoDB\Driver\Query(["_id" =>  $id]);
        $cursor = $manager->executeQuery('personal.departments', $query)->toArray();
        if ( count($cursor) == 0){           
            $record = array(
                '_id'  => $id,
                'name' => mb_convert_encoding($row['NAME'],'UTF-8','Windows-1251'),
                'short_name' => mb_convert_encoding($row['SHORT_NAME'],'UTF-8','Windows-1251'),
                "ancestors" => $ancestors,
                "parent" => $master,
                "isdiv"  => $isdiv,
                "checkin" => $now_bs,
                "isvalid"   => boolval(true),  
            );
            $bulk->insert($record);
                    
        } else {
             $bulk->update(["_id" => $id], array('$set' => [
                 "name" => mb_convert_encoding($row['NAME'],'UTF-8','Windows-1251'),
                 "short_name" => mb_convert_encoding($row['SHORT_NAME'],'UTF-8','Windows-1251'),
                 "isdiv"  => $isdiv,
		 "isvalid"   => boolval(true),
                 ])); 
        }
        
    }

    $sql = "SELECT post_no as dep_no FROM EXT_POST_STRUCTURE WHERE end_date >= TO_DATE(:stdate, 'YYYYMMDD') AND end_date <= sysdate ";
    $src = oci_parse($conn, $sql);
    oci_bind_by_name($src, ":stdate", $last_check);
    oci_execute($src);
    $mysql = "UPDATE departments SET isvalid = 0 WHERE id = :ID";
    //$mysql = "DELETE FROM departments WHERE id = :ID";
    $my = $mydb->prepare($mysql);

    while (($row = oci_fetch_array($src, OCI_ASSOC)) != false) {
        $id = intval($row['DEP_NO']);
        // $bulk->delete(["_id" =>  $id]);  
        $bulk->update(["_id" => $id], array('$set' => ["valid" => boolval(false)])); 
        $my->bindValue(':ID', $id, PDO::PARAM_INT);
        if(!$my->execute()){
            echo "Error update " . $id ."\n";
            print_r($mydb->errorInfo());
        } else { 
            $j = $j + $my->rowCount();
        }

    }

    $bulk->update(["_id" => intval(1)],  array('$set' => array("checkin" => $now_bs)));
    $result = $manager->executeBulkWrite('personal.departments', $bulk, $writeConcern);
    echo "Documents ::\n";
    echo " Inserted: ". $result->getInsertedCount() . "\n";
    echo " Updated : " . $result->getModifiedCount() . "\n";
    echo " Deleted : " . $result->getDeletedCount() . "\n";
    echo "Records   ::\n";
    echo " Ins/Upd : " . $i . "\n";
    echo " Deleted : " . $j . "\n";
    echo "---------------------------------------------------\n";
    echo "\n\n";