<?php
    include 'Config.php';
    error_reporting(E_ERROR); // только фатальные 

    // timestamp операции
    $now = new DateTime();
    $now_bs = new MongoDB\BSON\UTCDateTime($now);
    $now_db = $now->format('Ymd');

    $manager = new MongoDB\Driver\Manager('mongodb://'. Config::MON_USER . ':' . Config::MON_PWD . '@' . Config::MON_HOST);
	$bulk = new MongoDB\Driver\BulkWrite(['ordered' => true]);
    $writeConcern = new MongoDB\Driver\WriteConcern(MongoDB\Driver\WriteConcern::MAJORITY, 1000);

    $mydb = new PDO('mysql:host=slondike;dbname=UA', 'ua_user', 'superpuper');
    $mydb->exec("SET NAMES 'UTF8'");

    $sql = "SELECT `dep_short`, `dep_full`, `group_address`, `source_addresses`, `holder` FROM  UA.groupmail";
    foreach ($mydb->query($sql, PDO::FETCH_ASSOC) as $row){
        
        //$q_full = new MongoDB\BSON\Regex('^' . trim($row['dep_full']), 'i');
        //$q_short = new MongoDB\BSON\Regex('^' . trim($row['dep_short']), 'i');
        
        $q_full = mb_strtoupper(mb_substr($row['dep_full'], 0, 1)) . mb_substr($row['dep_full'], 1);
        $q_short = mb_strtoupper(trim($row['dep_short']));

        //echo $row['dep_short'] . ' ==> ' . $q_short  . PHP_EOL;
        $query = new MongoDB\Driver\Query(
            
                    ['$or' =>
                        [
                            ['short_name' =>  $q_short],
                            ['name' => $q_full]     
                        ]
                    ]
        );
        $cursor = $manager->executeQuery('personal.departments', $query)->toArray();
        if ( count($cursor) >= 1){
            echo '--> ' . $cursor['0']->_id . '  ' . $row['dep_short']  . '--' . $cursor['0']->short_name . PHP_EOL;
            $mails = explode(',', $row['source_addresses']);
            $mails = array_map('trim',$mails);

            unset($src_mails);
            foreach ($mails as $mail){
                
                $qr = new MongoDB\Driver\Query(['mail' => $mail],['projection' =>['_id'=>1, 'name_short'=>1]]);
                $src = $manager->executeQuery('personal.staff', $qr)->toArray();

                // Один email адрес может принадлежать :( нескольким работникам, конкатентим всех через разделитель
                $desc ='';
                $delimiter = '';
                foreach($src as $data){
                    $desc = $desc .  $delimiter . $data->name_short . '[' . $data->_id . ']'  ;
                    $delimiter = ';';
                }

                $src_mails[] = (object) array('mail' => $mail, 'desc' => $desc);

            }
                       
            $record = array(
                'group_mail'    => $row['group_address'],
                'group_name'    => $row['dep_full'],
                'group_id'      => $cursor['0']->_id,
                'sources'       => $src_mails
            );
            $bulk->insert($record);
        }

    }
    
            $z = $manager->executeBulkWrite('personal.group_mail', $bulk, $writeConcern);
            echo " Inserted: ". $z->getInsertedCount() . "\n";