<?php
include 'Config.php';
    error_reporting(E_ERROR); // только фатальные 

    /* Очистка и форматирование EMAIL */
function wmail($m){
    return filter_var($m,FILTER_SANITIZE_URL);
}
/* Очистка и форматирование телефонных номеров */
function wphone($m){
    return  preg_replace('/[^0-9+-]/', '', $m);
}

    // timestamp операции
    $now = new DateTime();
    $now_bs = new MongoDB\BSON\UTCDateTime($now);
    $now_db = $now->format('Ymd');
    $now_my = $now->format('Y-m-d');

    // connectors
    $conn = oci_connect(Config::ORA_USER,Config::ORA_PWD,Config::ORA_TNS,'CL8MSWIN1251');
	
	if (!$conn) {
		echo "No Conn to Oracle \n";
		echo $e['message'];
		$e = oci_error();
        trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);	
    }
	
    $manager = new MongoDB\Driver\Manager('mongodb://'. Config::MON_USER . ':' . Config::MON_PWD . '@' . Config::MON_HOST);
    

    $sql = "SELECT  personal_no as id
                , FIO as name
                , SHORT_FIO as name_short
                , div_no as dep_no
                , div_name as dep_name
                , born_date as born_date
                , category_no as cat_no
                , category_name as cat_name
                , profession_no as prof_no
                , profession_name as prof_name
                , work_email as mail
                , work_phone as wtel
                , start_date as sdate
            FROM  EXT_PERSONAL_NEW
            WHERE (CEH_NO = 23) AND (start_date >= TO_DATE(:stdate, 'YYYYMMDD')) AND ((end_date is null) OR (end_date > sysdate)) -- AND (PERSONAL_NO = 110486) ";
    // дата последней синхронизации (зависит от аргумента запуска)
      $query = new MongoDB\Driver\Query(["_id" =>  intval(1)]);
      $cursor = $manager->executeQuery('personal.staff', $query);
      foreach ($cursor as $doc){
          $last_check = $doc->checkin->toDateTime()->format('Ymd');
      }
      if (isset($argv[1])){
          if($argv[1] == 'full'){
              // Полное обновление, все записи старше 1900-01-01
              $last_check = '19000101';
          }
      }
      
      echo $now->format('Y-m-d e H:i:s') . "\n";
      echo "UA sync data, changed from: " . $last_check . "\n";
      echo "---------------------------------------------------\n";

    $bulk = new MongoDB\Driver\BulkWrite(['ordered' => true]);
    $wc = new MongoDB\Driver\WriteConcern(MongoDB\Driver\WriteConcern::MAJORITY, 1000);

    $src = oci_parse($conn, $sql);
    oci_bind_by_name($src, ":stdate", $last_check);
    oci_execute($src);
    while (($row = oci_fetch_array($src, OCI_ASSOC)) != false) {
        $k++;
        $id =  intval($row['ID']);
        $changes = false;
        // prepare data
        if (!empty($row['WTEL'])){
            //$wp = array_map('wphone', explode(',', $row['WTEL']));  //Телефоны могут быть введены через "," или "пробел"
            $wp = array_map('wphone', preg_split("/[\s,]+/",$row['WTEL']));
        } else { $wp = null; }
        if (!empty($row['MAIL'])){
            $wm = array_map('wmail', explode(',', $row['MAIL']));
        } else { $wm = null; }
        /*
        if (!empty($row['MTEL'])){
            $wmp = array_map('wphone', explode(',', $row['MTEL']));
        } else { $wmp = null; }
        */
        $record = array(
            '_id'   => intval($row['ID']),
            'name'  => mb_convert_encoding($row['NAME'],'UTF-8','Windows-1251'), 
            'name_short' => mb_convert_encoding($row['NAME_SHORT'],'UTF-8','Windows-1251'), 
            'dep_no'=> intval($row['DEP_NO']) ,
            'dep_name' => mb_convert_encoding($row['DEP_NAME'],'UTF-8','Windows-1251'),
            'cat_no' =>   intval($row['CAT_NO']),
            'cat_name'=> mb_convert_encoding($row['CAT_NAME'],'UTF-8','Windows-1251'),
            'prof_no'=> intval($row['PROF_NO']),
            'prof_name'=> mb_convert_encoding($row['PROF_NAME'],'UTF-8','Windows-1251'),
            'mail' => $wm,
            'wtel' => $wp, 
            'isvalid' => boolval(True),
            'checkin' => $now_bs, 
            'ad_acc'  => null,
            'wdep_no' => null,
            'wdep_name' => null,

        );
     

        // Запись или обновление в MONGODB 
		$query = new MongoDB\Driver\Query(["_id" =>  $id]);
        $cursor = $manager->executeQuery('personal.ua', $query)->toArray();
		if ( count($cursor) == 0){           
                // если не нашли вставляем запись
				$bulk->insert($record);
			} else {
                $curr_record = json_decode(json_encode($cursor[0]),true);  // такой способ преобразования объекта в массив
                // проверям на любые изменения
                $diff = array_diff_assoc($record, $curr_record);
                foreach($diff as $key => $value){
                    // Игнорируем изменения даты проверки, остальные изменения логгируем
                    /*
                    if ($key != 'checkin') {
                        $bulk_h->insert(array('pn'=> $id, 'name_short'=> $record['name_short'], 
                                                'changed'=>$key, 'oldvalue' => $curr_record[$key],
                                                'newvalue' => $record[$key])
                                        );
                                        echo "...hist rec to:" . $id . "\n";
                        $changes = true;
                    }
                    */
                    $bulk->update(["_id" => $id], array('$set' => [$key => $value])); 
                }
                // для рабочего и мобильного телефона отдельно, без логгирования. email не синхронизируем вообще.
                $diff = array_diff_assoc($record['wtel'], $curr_record['wtel']);
                if ($diff){
                    $bulk->update(["_id" => $id], array('$set' => ['wtel' => $wp]));
                    $changes = true;
                }
                $diff = array_diff_assoc($record['mtel'], $curr_record['mtel']);
                if ($diff){
                    $bulk->update(["_id" => $id], array('$set' => ['mtel' => $wp]));
                    $changes = true;
                }
        } 
        
    }
    // специальный документ, который хранит дату последнй операции
    $bulk->update(["_id" => intval(1)],  array('$set' => array("checkin" => $now_bs))); 
     
    $result = $manager->executeBulkWrite('personal.ua', $bulk, $wc);
    echo "New UA staff's ::::: \n";
    echo "  Doc Inserted: ". $result->getInsertedCount() . "\n";
    echo "  Doc Updated: " . $result->getModifiedCount() . "\n";
     
   
    // Пометка на удаление
    $changes = false;

    $bulk = new MongoDB\Driver\BulkWrite(['ordered' => true]);
    $wc = new MongoDB\Driver\WriteConcern(MongoDB\Driver\WriteConcern::MAJORITY, 1000);


    $sql = "SELECT personal_no as id FROM EXT_PERSONAL_DELO_P WHERE LV = 23 AND end_date >= TO_DATE(:stdate, 'YYYYMMDD') AND end_date <= sysdate ";
    $src = oci_parse($conn, $sql);
    oci_bind_by_name($src, ":stdate", $last_check);
    oci_execute($src);
    
    while (($row = oci_fetch_array($src, OCI_ASSOC)) != false) {
        $id = intval($row['ID']);
        $query = new MongoDB\Driver\Query(['$and' => [ ["_id" => $id],["isvalid" => boolval(true)] ] ]);
        $cursor = $manager->executeQuery('personal.ua', $query)->toArray();
		if ( count($cursor) > 0){           
            $bulk->update(["_id" => $id], array('$set' => ['isvalid' => boolval(false)])); 
            $bulk_h->insert(array('pn'=> $id, 'changed'=> 'isvalid', 'oldvalue' => true));
            $changes = true;
        }
        
    }
    
    echo "fired staff's ::::: \n";
    if ($changes) {
        try {
            $result = $manager->executeBulkWrite('personal.ua', $bulk, $wc);
            echo "  Doc mark deleted: " . $result->getModifiedCount() . "\n";
        } catch (MongoDB\Driver\Exception\BulkWriteException $e) {
            $result = $e->getWriteResult();
        }
    }
    
    echo "---------------------------------------------------\n";
    echo "\n\n";
