<?php
include 'Config.php';
    error_reporting(E_ERROR); // только фатальные 

    /* Очистка и форматирование EMAIL */
function wmail($m){
    return filter_var($m,FILTER_SANITIZE_URL);
}
/* Очистка и форматирование телефонных номеров */
function wphone($m){
    return  preg_replace('/[^0-9+-]/', '', $m);
}

    // timestamp операции
    $now = new DateTime();
    $now_bs = new MongoDB\BSON\UTCDateTime($now);
    $now_db = $now->format('Ymd');
    $now_my = $now->format('Y-m-d');

    // счетчики для MySQL
    $i = 0;
    $j = 0;
    $k = 0;

    // connectors
    $conn = oci_connect(Config::ORA_USER,Config::ORA_PWD,Config::ORA_TNS,'CL8MSWIN1251');
	
	if (!$conn) {
		echo "No Conn to Oracle \n";
		echo $e['message'];
		$e = oci_error();
        trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);	
    }
	
    $manager = new MongoDB\Driver\Manager('mongodb://'. Config::MON_USER . ':' . Config::MON_PWD . '@' . Config::MON_HOST);
    
	
    $mydb = new PDO('mysql:host='. Config::MY_HOST, Config::MY_USER, Config::MY_PWD);
	if($mydb){
		$mydb->exec("SET NAMES 'UTF8'");
		} else {
		echo "No connect to mysql";
	}


    $sql = "SELECT  personal_no as id
                    , FIO as name
                    , SHORT_FIO as name_short
                    , div_no as dep_no
                    , div_name as dep_name
                    , lv as div_no
                    , nlv as div_name
                    , cex_no as wrk_no
                    , cex_name as wrk_name
                    , category_no as cat_no
                    , category_name as cat_name
					, team_no as team_no
                    , profession_no as prof_no
                    , profession_name as prof_name
                    , work_email as mail
                    , work_phone as wtel
                    , mobil_phone as mtel
                    , start_date as sdate
            FROM  EXT_PERSONAL_DELO_P 
            WHERE (start_date >= TO_DATE(:stdate, 'YYYYMMDD')) AND ((end_date is null) OR (end_date > sysdate))";
    
    $mysql = "INSERT INTO staff 
        (_id,`name`,name_short,dep_no,dep_name,div_no,div_name,wrk_no,wrk_name,cat_no,cat_name,prof_no,prof_name,mail,wtel,mtel,checkin,isvalid,sdate)
        VALUES (:ID,:NAME,:NAMES,:DEP,:DEPN,:DIV,:DIVN,:WRK,:WRKN,:CAT,:CATN,:PROF,:PROFN,:MAIL,:WTEL,:MTEL,:CHI,:VALID,:SDATE)
        ON DUPLICATE KEY UPDATE `name` = :NAME
        ,name_short=:NAMES
        ,dep_no=:DEP
        ,dep_name=:DEPN
        ,div_no=:DIV
        ,div_name=:DIVN
        ,wrk_no=:WRK
        ,wrk_name=:WRKN
        ,cat_no=:CAT
        ,cat_name=:CATN
		,team_no=:TEAM_NO
        ,prof_no=:PROF
        ,prof_name=:PROFN
        ,mail=:MAIL
        ,wtel=:WTEL
        ,mtel=:MTEL
        ,sdate=:SDATE";
    $my = $mydb->prepare($mysql);
		echo "get Data..." ;
      // дата последней синхронизации (зависит от аргумента запуска)
      $query = new MongoDB\Driver\Query(["_id" =>  intval(1)]);
      $cursor = $manager->executeQuery('personal.staff', $query);
      foreach ($cursor as $doc){
          $last_check = $doc->checkin->toDateTime()->format('Ymd');
      }
      if (isset($argv[1])){
          if($argv[1] == 'full'){
              // Полное обновление, все записи старше 1900-01-01
              $last_check = '19000101';
          }
      }
      
      echo $now->format('Y-m-d e H:i:s') . "\n";
      echo "STAFF sync data, changed from: " . $last_check . "\n";
      echo "---------------------------------------------------\n";

    $bulk = new MongoDB\Driver\BulkWrite(['ordered' => true]);
    $bulk_h = new MongoDB\Driver\BulkWrite(['ordered' => true]);
    $wc = new MongoDB\Driver\WriteConcern(MongoDB\Driver\WriteConcern::MAJORITY, 1000);
    $wc_h = new MongoDB\Driver\WriteConcern(MongoDB\Driver\WriteConcern::MAJORITY, 1000);

    $src = oci_parse($conn, $sql);
    oci_bind_by_name($src, ":stdate", $last_check);
    oci_execute($src);
    while (($row = oci_fetch_array($src, OCI_ASSOC)) != false) {
        $k++;
        $id =  intval($row['ID']);
        $changes = false;
        // prepare data
        if (!empty($row['WTEL'])){
            //$wp = array_map('wphone', explode(',', $row['WTEL']));  //Телефоны могут быть введены через "," или "пробел"
            $wp = array_map('wphone', preg_split("/[\s,]+/",$row['WTEL']));
        } else { $wp = null; }
        if (!empty($row['MAIL'])){
            $wm = array_map('wmail', explode(',', $row['MAIL']));
        } else { $wm = null; }
        if (!empty($row['MTEL'])){
            $wmp = array_map('wphone', explode(',', $row['MTEL']));
        } else { $wmp = null; }

        $record = array(
            '_id'   => intval($row['ID']),
            'name'  => mb_convert_encoding($row['NAME'],'UTF-8','Windows-1251'), 
            'name_short' => mb_convert_encoding($row['NAME_SHORT'],'UTF-8','Windows-1251'), 
            'dep_no'=> intval($row['DEP_NO']) ,
            'dep_name' => mb_convert_encoding($row['DEP_NAME'],'UTF-8','Windows-1251'),
            'div_no' => intval($row['DIV_NO']),
            'div_name' => mb_convert_encoding($row['DIV_NAME'],'UTF-8','Windows-1251'),
            'wrk_no' => intval($row['WRK_NO']),           
            'wrk_name'=> mb_convert_encoding($row['WRK_NAME'],'UTF-8','Windows-1251'),
            'cat_no' =>   intval($row['CAT_NO']),
            'cat_name'=> mb_convert_encoding($row['CAT_NAME'],'UTF-8','Windows-1251'),
			'team_no' => intval($row['TEAM_NO']),
            'prof_no'=> intval($row['PROF_NO']),
            'prof_name'=> mb_convert_encoding($row['PROF_NAME'],'UTF-8','Windows-1251'),
            'mail' => $wm,
            'wtel' => $wp, 
            'mtel' => $wmp,
            'isvalid' => boolval(True),
            'checkin' => $now_bs, 
            //'ad_acc'  => null,
        );
     
        $my->bindValue(':ID', intval($row['ID']), PDO::PARAM_INT);
        $my->bindValue(':NAME', mb_convert_encoding($row['NAME'],'UTF-8','Windows-1251'), PDO::PARAM_STR);
        $my->bindValue(':NAMES', mb_convert_encoding($row['NAME_SHORT'],'UTF-8','Windows-1251'), PDO::PARAM_STR);
        $my->bindValue(':DEP', intval($row['DEP_NO']), PDO::PARAM_INT);
        $my->bindValue(':DEPN', mb_convert_encoding($row['DEP_NAME'],'UTF-8','Windows-1251'), PDO::PARAM_STR);
        $my->bindValue(':DIV', intval($row['DIV_NO']), PDO::PARAM_INT);
        $my->bindValue(':DIVN', mb_convert_encoding($row['DIV_NAME'],'UTF-8','Windows-1251'), PDO::PARAM_STR);
        $my->bindValue(':WRK', intval($row['WRK_NO']), PDO::PARAM_INT);
        $my->bindValue(':WRKN', mb_convert_encoding($row['WRK_NAME'],'UTF-8','Windows-1251'), PDO::PARAM_STR);
        $my->bindValue(':CAT', intval($row['CAT_NO']), PDO::PARAM_INT);
        $my->bindValue(':CATN', mb_convert_encoding($row['CAT_NAME'],'UTF-8','Windows-1251'), PDO::PARAM_STR);
		$my->bindValue(':TEAM_NO', intval($row['TEAM_NO']), PDO::PARAM_INT);
        $my->bindValue(':PROF', intval($row['PROF_NO']), PDO::PARAM_INT);
        $my->bindValue(':PROFN', mb_convert_encoding($row['PROF_NAME'],'UTF-8','Windows-1251'), PDO::PARAM_STR);
        $my->bindValue(':MAIL', json_encode($wm), PDO::PARAM_STR);
        $my->bindValue(':WTEL', json_encode($wp), PDO::PARAM_STR);
        $my->bindValue(':MTEL', json_encode($wmp), PDO::PARAM_STR);
        $my->bindValue(':CHI', $now_my, PDO::PARAM_STR);
        $my->bindValue(':VALID', 1, PDO::PARAM_INT);
        $my->bindValue(':SDATE', DateTime::createFromFormat('d-M-y',$row['SDATE'])->format('Y-m-d'), PDO::PARAM_STR);
        // Запись или обновление в MySQL
        if(!$my->execute()){
            echo "Error update PN=" . $id ."\n";
        } else { 
            $i = $i + $my->rowCount();
        }

        // Запись или обновление в MONGODB 
		$query = new MongoDB\Driver\Query(["_id" =>  $id]);
        $cursor = $manager->executeQuery('personal.staff', $query)->toArray();
		if ( count($cursor) == 0){           
                // если не нашли вставляем запись
				$bulk->insert($record);
			} else {
                $curr_record = json_decode(json_encode($cursor[0]),true);  // такой способ преобразования объекта в массив
                // проверям на любые изменения
                $diff = array_diff_assoc($record, $curr_record);
                foreach($diff as $key => $value){
                    // Игнорируем изменения даты проверки, остальные изменения логгируем
                    if ($key != 'checkin') {
                        $bulk_h->insert(array('pn'=> $id, 'name_short'=> $record['name_short'], 
                                                'changed'=>$key, 'oldvalue' => $curr_record[$key],
                                                'newvalue' => $record[$key])
                                        );
                                        // echo "...hist rec to:" . $id . "\n";
                        $changes = true;
                    }
                    $bulk->update(["_id" => $id], array('$set' => [$key => $value])); 
                }
                // для рабочего и мобильного телефона отдельно, без логгирования. Email c логгированием
                $diff = array_diff_assoc($record['wtel'], $curr_record['wtel']);
                if ($diff){
                    $bulk->update(["_id" => $id], array('$set' => ['wtel' => $wp]));
                    $changes = false;
                }
                $diff = array_diff_assoc($record['mtel'], $curr_record['mtel']);
                if ($diff){
                    $bulk->update(["_id" => $id], array('$set' => ['mtel' => $wp]));
                    $changes = false;
                }
				$diff = array_diff_assoc($record['mail'], $curr_record['mail']);
                if ($diff){
                    $bulk->update(["_id" => $id], array('$set' => ['mail' => $wm]));
					$bulk_h->insert(array('pn'=> $id, 'name_short'=> $record['name_short'], 
                                                'changed'=>mail, 'oldvalue' => $curr_record['mail'],
                                                'newvalue' => $record['mail'])
                                        );
                    $changes = true;
                }

        } 
        
    }
    // специальный документ, который хранит дату последнй операции
    $bulk->update(["_id" => intval(1)],  array('$set' => array("checkin" => $now_bs))); 
     
    $result = $manager->executeBulkWrite('personal.staff', $bulk, $wc);
    echo "New staff's ::::: \n";
    echo "  Doc Inserted: ". $result->getInsertedCount() . "\n";
    echo "  Doc Updated: " . $result->getModifiedCount() . "\n";
    if ($changes) {
        $history = $manager->executeBulkWrite('personal.staff_history', $bulk_h, $wc_h);
        echo "  Hist Doc: " . $history->getInsertedCount() . "\n";
    }
    echo "  Rec Ins/Upd: " . $i . "\n";
    echo "  SRC records: " . $k . "\n\n\n";
    

    
    // Пометка на удаление
    $changes = false;
    $j = 0;

    $bulk = new MongoDB\Driver\BulkWrite(['ordered' => true]);
    $wc = new MongoDB\Driver\WriteConcern(MongoDB\Driver\WriteConcern::MAJORITY, 1000);
    $bulk_h = new MongoDB\Driver\BulkWrite(['ordered' => true]);
    $wc_h = new MongoDB\Driver\WriteConcern(MongoDB\Driver\WriteConcern::MAJORITY, 1000);

    $sql = "SELECT personal_no as id FROM EXT_PERSONAL_DELO_P WHERE end_date >= TO_DATE(:stdate, 'YYYYMMDD') AND end_date <= sysdate ";
    $src = oci_parse($conn, $sql);
    oci_bind_by_name($src, ":stdate", $last_check);
    oci_execute($src);
    $mysql = "UPDATE staff SET isvalid = 0 WHERE _id = :ID";
    $my = $mydb->prepare($mysql);
    while (($row = oci_fetch_array($src, OCI_ASSOC)) != false) {
        $id = intval($row['ID']);
        $query = new MongoDB\Driver\Query(['$and' => [ ["_id" => $id],["isvalid" => boolval(true)] ] ]);
        $cursor = $manager->executeQuery('personal.staff', $query)->toArray();
		if ( count($cursor) > 0){           
            $bulk->update(["_id" => $id], array('$set' => ['isvalid' => boolval(false)])); 
            $bulk_h->insert(array('pn'=> $id, 'changed'=> 'isvalid', 'oldvalue' => true));
            $changes = true;
        }
        $my->bindValue(':ID', $id, PDO::PARAM_INT);
        if(!$my->execute()){
            // echo "Error delete " . $id ."\n";
        } else { 
            $j = $j + $my->rowCount();
        }
    }
    
    echo "InValid staff's ::::: \n";
    if ($changes) {
        try {
            $result = $manager->executeBulkWrite('personal.staff', $bulk, $wc);
            echo "  Doc mark deleted: " . $result->getModifiedCount() . "\n";
            $history = $manager->executeBulkWrite('personal.staff_history', $bulk_h, $wc_h);
        } catch (MongoDB\Driver\Exception\BulkWriteException $e) {
            $result = $e->getWriteResult();
            echo "  Doc add hist: " . $history->getInsertedCount() . "\n";
        }
    }
    
    echo "  Rec mark deleted: " . $j . "\n";
    echo "---------------------------------------------------\n";
    echo "\n\n";
